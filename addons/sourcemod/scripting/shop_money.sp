#pragma semicolon 1
#include <shop>
#include <cstrike>
#include <hlstatsX_adv>
#pragma newdecls required

int g_iAccount = -1;

public Plugin myinfo =
{
 name = "[Shop] money",
 author = "ShaRen",
 description = "Money = credits",
 version =  "1.0.1"
}

public void OnPluginStart()
{
	g_iAccount = FindSendPropOffs("CCSPlayer", "m_iAccount");
	CreateTimer(5.0, Update, _, TIMER_REPEAT);
	
	if (g_iAccount == -1)
		SetFailState("[CS:GO] Give Cash - Failed to find offset for m_iAccount!");
}


public Action Update (Handle timer)
{
	for (int i = 1, time=0; i < MAXPLAYERS; i++)
		if (IsClientInGame(i) && !IsFakeClient(i)) {
			SetEntData(i, g_iAccount, Shop_GetClientCredits(i));
			time = GetClientPlayedTime(i);
			if (time > 0)
				CS_SetMVPCount(i, RoundToFloor( SquareRoot(float(time))/30 ));
		}
}

public Action Shop_OnCreditsGiven(int client, int &credits, int by_who)
{
	SetEntData(client, g_iAccount, Shop_GetClientCredits(client)+credits);
}

public Action Shop_OnCreditsTaken(int client, int &credits, int by_who)
{
	SetEntData(client, g_iAccount, Shop_GetClientCredits(client)-credits);
}
